package Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class ServerClientThread implements Runnable {
    private final Socket socket;
    private final OutputStreamWriter writer;
    private final BufferedReader reader;
    private String nickName;
    private String clientValue;
    private String time;
    private String userMessage;
    private String nickNameOtherUser = "";
    private static final Map<String, ServerClientThread> clientThreadMap = new ConcurrentHashMap<>();
    private static final AtomicInteger accountOfConnection = new AtomicInteger(0);
    private static final Integer MAX_CONNECTION = 3;

    public ServerClientThread(Socket socket) throws IOException {
        this.socket = socket;
        this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), StandardCharsets.UTF_8));
        this.writer = new OutputStreamWriter(socket.getOutputStream(), StandardCharsets.UTF_8);
    }

    @Override
    public void run() {
        try {
            while (true) {
                if (accountOfConnection.get() >= MAX_CONNECTION) {
                    writer.append("Connection limit exceeded.\n");
                    writer.flush();
                    Thread.sleep(2000);
                    continue;
                } else {
                    accountOfConnection.set(accountOfConnection.get() + 1);
                    break;
                }
            }
        } catch (Exception e) {
        }
        try {
            while (true) {
                writer.append("Enter your nickName:\n");
                writer.flush();
                clientValue = reader.readLine();
                time = clientValue.substring(clientValue.length() - 10);
                nickName = clientValue.substring(0, clientValue.length() - 10);
                if ("exit".equals(nickName)) {
                    accountOfConnection.set(accountOfConnection.get() - 1);
                    socket.close();
                    return;
                }
                if (clientThreadMap.containsKey(nickName)) {
                    writer.append(time + " " + "User named " + nickName + " already exists.\n");
                    continue;
                } else {
                    break;
                }

            }
        } catch (IOException e) {
            accountOfConnection.set(accountOfConnection.get() - 1);
        }
        Thread.currentThread().setName(nickName);
        clientThreadMap.put(nickName, this);
        System.out.println("New connection is in thread " + Thread.currentThread().getName());
        for (Map.Entry<String, ServerClientThread> element : clientThreadMap.entrySet()) {
            element.getValue().sendMessage(time + " " + "User " + nickName + " is online.");
        }
        try {
            while (true) {
                clientValue = reader.readLine();
                time = clientValue.substring(clientValue.length() - 10);
                userMessage = clientValue.substring(0, clientValue.length() - 10);
                if (userMessage == null || userMessage.isEmpty()) {
                    continue;
                }
                writeMessagesLog();
                if ("exit".equals(userMessage)) {
                    for (Map.Entry<String, ServerClientThread> element : clientThreadMap.entrySet()) {
                        element.getValue().sendMessage(time + " " + "User " + nickName + " is offline.");
                    }
                    clientThreadMap.remove(nickName);
                    accountOfConnection.set(accountOfConnection.get() - 1);
                    break;
                }
                if (userMessage.startsWith("*")) {
                    choicePrivatePublicMessages();
                    continue;
                }
                sendPackageOfMessages();
            }
        } catch (Exception ex) {
            clientThreadMap.remove(nickName);
            accountOfConnection.set(accountOfConnection.get() - 1);
            for (Map.Entry<String, ServerClientThread> element : clientThreadMap.entrySet()) {
                element.getValue().sendMessage(time + " " + "User " + nickName + " is offline.");
            }
        }
    }

    private void sendMessage(String msg) {
        try {
            writer.append(msg + "\n");
            writer.flush();
        } catch (IOException ignored) {
        }
    }

    private void sendPackageOfMessages() {
        for (Map.Entry<String, ServerClientThread> element : clientThreadMap.entrySet()) {
            if (nickNameOtherUser.equals("")) {
                element.getValue().sendMessage(time + " " + nickName +
                        ": " + userMessage);
            } else if (element.getKey().equals(nickNameOtherUser) || nickName.equals(element.getKey())) {
                element.getValue().sendMessage(time + " " + nickName + " -> " + nickNameOtherUser + ": "
                        + userMessage);
            }
        }
    }

    private void choicePrivatePublicMessages() {
        nickNameOtherUser = userMessage.substring(1);
        if (!nickNameOtherUser.equals("")) {
            if (!clientThreadMap.containsKey(nickNameOtherUser)) {
                this.sendMessage(time + " " + "Name '" + nickNameOtherUser + "' doesn't exist. Enter other name otherwise you will send message to everybody.");
                nickNameOtherUser = "";
            } else {
                this.sendMessage(time + " " + "You will send message to " + nickNameOtherUser + ".");
            }
        } else {
            this.sendMessage(time + " " + "You will send a message to everybody.");
        }
    }

    private void writeMessagesLog() {
        if (userMessage.startsWith("*")) {
            return;
        }
        if (!nickNameOtherUser.equals("")) {
            System.out.println("From client " + nickName + " to " + nickNameOtherUser + ": " + userMessage);
        } else {
            System.out.println("From client " + nickName + " to everybody: " + userMessage);
        }
    }
}

